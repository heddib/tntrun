package fr.heddib.games.tntrun;

import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;

import fr.heddib.core.HeddiBPlugin;
import fr.heddib.core.command.CommandCenter;
import fr.heddib.core.util.TimeUnit;
import fr.heddib.core.util.Txt;
import fr.heddib.games.tntrun.core.GameManager;
import fr.heddib.games.tntrun.core.user.UserManager;
import fr.heddib.games.tntrun.database.ConfigLoader;
import fr.heddib.games.tntrun.messages.MessageHandler;

public class TntRun extends HeddiBPlugin {
	
	    // -------------------------------------------- //
		// COMMON CONSTANTS
		// -------------------------------------------- //
		
		public final static String INSTANCE = "instance";
		public final static String DEFAULT = "default";
		public static final String NONE = Txt.parse("<silver>none");
		
		// -------------------------------------------- //
		// INSTANCE & CONSTRUCT
		// -------------------------------------------- //
		
		private static TntRun i;
		public static TntRun get() { return i; }

		private GamePlugin game;
		public TntRun() { i = this; }
		
		// -------------------------------------------- //
		// STATIC
		// -------------------------------------------- //
		
		public static FileConfiguration messages;
		public static GameManager gameManager;
		public static UserManager userManger;
		
		// -------------------------------------------- //
		// OVERRIDE
		// -------------------------------------------- //
		
		@Override
		public void onLoad()
		{
			super.onLoad();
			System.out.println("TimeUnit.MILLIS_PER_MINUTE: " + TimeUnit.MILLIS_PER_MINUTE);
		}
		
		@Override
		public void onEnable()
		{
			
			if ( ! preEnable()) return;
			
			if(!Bukkit.getPluginManager().isPluginEnabled("HeddiBCore")) {
				System.err.println("[TNTRun] #########################################################");
				System.err.println("[TNTRun] ######## NO HEDDIBCORE FOUND! DISABLE PLUGIN... #########");
				System.err.println("[TNTRun] #########################################################");
				Bukkit.getPluginManager().disablePlugin(this);
				return;
			}
			
			// Config
			new ConfigLoader().load();
			
			// Commands
			CommandCenter.Initialize(i);
			
			//Game.reinitializeDatabase();
			MessageHandler.reload();
			
			gameManager = new GameManager();
			userManger = new UserManager();
			
			game = new GamePlugin(this);
			
			this.postEnable();
		}
		
		@Override
		public void onDisable()
		{
			super.onDisable();
		}
		
		// -------------------------------------------- //
		// CONFIG
		// -------------------------------------------- //

		public static void saveMessages() {
			try {
				messages.save("plugins/TNTRun/messages.yml");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		public GamePlugin getGame() {
			return game;
		}
		
}
