package fr.heddib.games.tntrun.core;

import fr.heddib.core.game.Game;
import fr.heddib.core.game.GameState;
import fr.heddib.games.tntrun.TntRun;

public class ResetPhase {
	
	private Game game;
	
	public ResetPhase(Game game) {
		this.game = game;
		start();
	}
	
	public void start() {
		game.kickall();
		game.setState(GameState.RESET);
	    TntRun.get().getGame().regenNow();
	    String name = game.getName();
		TntRun.gameManager.unload(game);
		TntRun.gameManager.load(name);
	}

}
