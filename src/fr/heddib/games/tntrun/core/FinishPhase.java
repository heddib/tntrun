package fr.heddib.games.tntrun.core;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitTask;

import fr.heddib.core.Util;
import fr.heddib.core.game.Game;
import fr.heddib.core.game.GamePhase;
import fr.heddib.core.game.GameState;
import fr.heddib.games.tntrun.TntRun;
import fr.heddib.games.tntrun.messages.MessageHandler;

public class FinishPhase extends GamePhase {

	private Game game;
	private int time = 10;
	private BukkitTask task;
	
	public FinishPhase(Game game) {
		super(game);
		this.game = game;
	}
	
	public void load() {
		game.announceEnd(game.getPlacements());
		game.setState(GameState.END);
		game.updateScoreboard();
		game.getUsers().get(0).getPlayer().setAllowFlight(true);
		game.getUsers().get(0).getPlayer().setFlying(true);
		task = Bukkit.getScheduler().runTaskTimer(TntRun.get(), new Runnable() {
			public void run() {
				if(time == 0) {
					game.getUsers().get(0).getPlayer().setAllowFlight(false);
					game.getUsers().get(0).getPlayer().setFlying(false);
					task.cancel();
					game.end();
					return;
				} else {
					if(game.getUsers().size() > 0) {
						Util.shootRandomFirework(game.getUsers().get(0).getPlayer().getLocation(), 1);
					}
				}
				
				if(time == 5) {
					game.sendMessage(MessageHandler.getMessage("game-end").replace("%0%", Util.getFormatedTime(time)));
				}
				time--;
			}
		}, 20L, 20L);
	}
	
	public void cancelTask() {
		if(task != null) {
			task.cancel();
		}
	}

	// Rien � faire...
	public void start() {}

}
