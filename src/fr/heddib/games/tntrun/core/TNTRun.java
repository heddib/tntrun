package fr.heddib.games.tntrun.core;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.Scoreboard;

import fr.heddib.core.HeddiBCore;
import fr.heddib.core.Util;
import fr.heddib.core.api.TitleAPI;
import fr.heddib.core.event.UpdateEvent;
import fr.heddib.core.event.UpdateType;
import fr.heddib.core.game.GameState;
import fr.heddib.core.game.SoloGame;
import fr.heddib.core.game.events.UserLobbyJoinedEvent;
import fr.heddib.core.game.user.SpectatorUser;
import fr.heddib.core.game.user.User;
import fr.heddib.core.util.C;
import fr.heddib.core.util.UtilServer;
import fr.heddib.games.tntrun.TntRun;
import fr.heddib.games.tntrun.core.scoreboard.GameScoreboard;
import fr.heddib.games.tntrun.messages.MessageHandler;

public class TNTRun extends SoloGame {
	
	private CooldownPhase cooldownPhase;
	private IngamePhase ingamePhase;
	private FinishPhase finishPhase;
	private GameScoreboard scoreboard;
	
	public TNTRun(Location lobby) {
		super("TNTRun", new String[]{"&cTNTRun", "&eCourrez sans vous arr�ter...", "&eLe sol tombe sous vos pieds"}, 10, 20, lobby);
		
		addGamePhase(cooldownPhase);
		addGamePhase(ingamePhase);
		addGamePhase(finishPhase);
		
		UtilServer.getServer().getPluginManager().registerEvents(this, TntRun.get());
		
		this.cooldown = 10;
		this.scoreboard = new GameScoreboard(this);
	}

	private boolean forcedStart = false;
	public void checkForStart() {
		if(getUsers().size() == getRequiredPlayers() || forcedStart) {
			if(cooldownPhase != null && cooldownPhase.isRunning())
				return;
			
		    startCooldown();
		}
	}
	
	public void checkForCancelStart() {
		if(getState() != GameState.COOLDOWN)
			return;
		
		if(forcedStart) {
			if(getUsers().size() == 1) {
				if(getState() == GameState.COOLDOWN) {
					cooldownPhase.cancelTask();
					sendMessage(MessageHandler.getMessage("game-start-canceled"));
				}
				forcedStart = false;
			}
			
		} else {
			if(getUsers().size() == getRequiredPlayers() - 1) {
				if(getState() == GameState.COOLDOWN) {
					cooldownPhase.cancelTask();
					sendMessage(MessageHandler.getMessage("game-start-canceled"));
				}
			}
		}
	}

	public void join(User user) {
		getUsers().add(user);
		Player p = user.getPlayer();
		p.setScoreboard(this.scoreboard.GetScoreboard());
		for(Player lel : Bukkit.getOnlinePlayers()) {
			p.showPlayer(lel);
		}
		
		TitleAPI.sendTabTitle(p, "&c&l- TNTRUN -", "&d*o* Vous �tes sur &a&lCraft&c&lFor&e&lAdventure &d*o*");
		
		p.teleport(getLobby());
		
		user.clear();
		p.getInventory().setItem(7, getLeaveItem());
		p.updateInventory();
		
		sendMessage(MessageHandler.getMessage("join-success").replace("%0%", p.getName()).replace("%1%", Integer.valueOf(getUsers().size()).toString()).replace("%2%", Integer.valueOf(getMaximumPlayers()).toString()));
		updateScoreboard();
		checkForStart();
//		updateBossBarMessage();
		Bukkit.getPluginManager().callEvent(new UserLobbyJoinedEvent(user, this));
	}
	
	public void joinSpectator(final SpectatorUser user) {
		getSpectators().add(user);
	    user.getPlayer().teleport(getLobby());
		
		for(User u : getUsers()) {
			u.getPlayer().hidePlayer(user.getPlayer());
		}
	
		user.clear();
		user.getPlayer().setAllowFlight(true);
		user.getPlayer().setFlying(true);
		user.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 1000000, 1));
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(HeddiBCore.get(), new Runnable() {
			public void run() {
				user.getPlayer().getInventory().setItem(8, getLeaveItem());
				user.getPlayer().getInventory().setItem(7, getPlayerNavigatorItem());
				user.getPlayer().updateInventory();
			}
		}, 2L);
		
		sendSpectators(MessageHandler.getMessage("spectator-join").replace("%0%", user.getName()));
	}

	public void startCooldown() {
		TntRun.get().log(getCooldownTime());
		cooldownPhase = new CooldownPhase(this);
		cooldownPhase.load();
	}

	public void startIngame() {
		ingamePhase = new IngamePhase(this);
		ingamePhase.load();
	}

	public void kickall() {
		if(getUsers().size() != 0) {
			int size = getUsers().size();
			for(int i = 0; i < size; i++) {
				try {
					TntRun.userManger.leaveGame(getUsers().get(i).getPlayer());
				} catch(IndexOutOfBoundsException e) {
					break;
				}
			}
		}
		if(getSpectators().size() != 0) {
			int size = getSpectators().size();
			for(int i = 0; i < size; i++) {
				try {
					TntRun.userManger.leaveGame(getSpectators().get(i));
				} catch(IndexOutOfBoundsException e) {
					break;
				}
			}
		}
	}

	public void startFinish() {
		finishPhase = new FinishPhase(this);
		finishPhase.load();
	}
	
	public boolean isFinishing() {
		return finishPhase != null;
	}

	public void end() {
		new ResetPhase(this);
	}

	public void forceStart(Player p) {
		if(getUsers().size() < 2) {
			p.sendMessage(MessageHandler.getMessage("prefix") + "�cAt least 2 players are required to start the game!");
			return;
		}
		
		if((cooldownPhase != null && cooldownPhase.isRunning()) ) {
			p.sendMessage(MessageHandler.getMessage("prefix") + "�cThe game is already starting!");
			return;
		}
		
		forcedStart = true;
		checkForStart();
		p.sendMessage(MessageHandler.getMessage("prefix") + "You've started the game in lobby " + getName() + " successfully!");
	}
	
	public IngamePhase getIngamePhrase() {
		return ingamePhase;
	}
	
	public CooldownPhase getCooldownPhrase() {
		return cooldownPhase;
	}

	public GameScoreboard getGameScoreboard() {
		return scoreboard;
	}
	
	public Scoreboard getScoreboard() {
		return scoreboard.GetScoreboard();
	}
	
	/*@EventHandler
	  public void scoreboardUpdate(UpdateEvent event)
	  {
	    if (!(getState() == GameState.WAITING || getState() == GameState.COOLDOWN || getState() == GameState.INGAME)) {
	      return;
	    }
	    if (event.getType() != UpdateType.FAST) {
	      return;
	    }
	    writeScoreboard();
	  }*/
	
	@EventHandler
	  public void scoreboardTitleUpdate(UpdateEvent event)
	  {
	    if (event.getType() != UpdateType.FASTEST) {
	      return;
	    }
	    this.scoreboard.UpdateTitle();
	  }
	  
	  public void writeScoreboard()
	  {
	    this.scoreboard.Reset();

	    this.scoreboard.WriteBlank();
	    this.scoreboard.Write(C.cYellow + "Serveur: " + TntRun.get().getConfig().getString("server"));    

	    this.scoreboard.WriteBlank();
	    if (getState() == GameState.WAITING)
	    {
	      this.scoreboard.Write(C.cGreen + "En attente...");
	      this.scoreboard.WriteBlank();
	      this.scoreboard.Write(C.cYellow + "Joueurs: " + getUsers().size());
	    }
	    else if (getState() == GameState.COOLDOWN)
	    {
	      this.scoreboard.Write(C.cYellow + "D�marrage dans");
	      this.scoreboard.Write(C.cBlue + Util.getFormatedTime(cooldownPhase.getTime()));
	    }
	    else if (getState() == GameState.INGAME)
	    {
	    	this.scoreboard.Write(C.cGreen + "En jeu");
		    this.scoreboard.WriteBlank();
		    this.scoreboard.Write(C.cYellow + "Joueurs: " + getUsers().size());
	    }
	    else if (getState() == GameState.END)
	    {
	    	this.scoreboard.Write(C.cGreen + "Fin !");
		    this.scoreboard.WriteBlank();
		    this.scoreboard.Write(C.cYellow + "Gagnant: " + getUsers().get(0).getName());
	    }
	    this.scoreboard.WriteBlank();
	    this.scoreboard.Write(C.cGold + "play.mc-cfa.tk");
	    this.scoreboard.Draw();
	  }
	  
	  public void annonceEnd() {}

	@Override
	public void updateScoreboard() {
		writeScoreboard();
	}

}
