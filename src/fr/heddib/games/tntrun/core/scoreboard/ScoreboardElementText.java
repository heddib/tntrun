package fr.heddib.games.tntrun.core.scoreboard;

import java.util.ArrayList;

public class ScoreboardElementText extends ScoreboardElement {
	
	  private String _line;
	  
	  public ScoreboardElementText(String line)
	  {
	    this._line = line;
	  }
	  
	  public ArrayList<String> GetLines()
	  {
	    ArrayList<String> orderedScores = new ArrayList<>();
	    
	    orderedScores.add(this._line);
	    
	    return orderedScores;
	  }

}
