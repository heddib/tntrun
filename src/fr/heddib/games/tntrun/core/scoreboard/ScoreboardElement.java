package fr.heddib.games.tntrun.core.scoreboard;

import java.util.ArrayList;

public abstract class ScoreboardElement
{
  public abstract ArrayList<String> GetLines();
}
