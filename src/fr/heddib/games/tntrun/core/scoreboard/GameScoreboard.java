package fr.heddib.games.tntrun.core.scoreboard;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import fr.heddib.core.game.Game;
import fr.heddib.core.util.HUtil;
import fr.heddib.core.util.UtilMath;

public class GameScoreboard
{
  @SuppressWarnings("unused")
private Game Game;
  private Scoreboard _scoreboard;
  private Objective _sideObjective;
  private ArrayList<ScoreboardElement> _elements = new ArrayList<>();
  private String[] _current = new String[15];
  private String _title;
  private int _shineIndex;
  private boolean _shineDirection = true;
  private boolean _debug = false;
  
  public GameScoreboard(Game game)
  {
    this.Game = game;
    
    this._title = "   TNTRUN   ";
    

    this._scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
    
    this._sideObjective = this._scoreboard.registerNewObjective("Obj" + UtilMath.r(999999999), "dummy");
    this._sideObjective.setDisplaySlot(DisplaySlot.SIDEBAR);
    this._sideObjective.setDisplayName(HUtil.colorize("&l" + this._title));
  }
  
  public Scoreboard GetScoreboard()
  {
    return this._scoreboard;
  }
  
  public Objective GetObjectiveSide()
  {
    return this._sideObjective;
  }
  
  public void UpdateTitle()
  {
    String out;
    if (this._shineDirection) {
      out = "&6&l";
    } else {
      out = "&f&l";
    }
    for (int i = 0; i < this._title.length(); i++)
    {
      char c = this._title.charAt(i);
      if (this._shineDirection)
      {
        if (i == this._shineIndex) {
          out = out + "&e&l";
        }
        if (i == this._shineIndex + 1) {
          out = out + "&f&l";
        }
      }
      else
      {
        if (i == this._shineIndex) {
          out = out + "&e&l";
        }
        if (i == this._shineIndex + 1) {
          out = out + "&6&l";
        }
      }
      out = out + c;
    }
    this._sideObjective.setDisplayName(HUtil.colorize(out));
    
    this._shineIndex += 1;
    if (this._shineIndex == this._title.length() * 2)
    {
      this._shineIndex = 0;
      this._shineDirection = (!this._shineDirection);
    }
  }
  
  public String ParseTeamName(String name)
  {
    return name.substring(0, Math.min(16, name.length()));
  }

  public void ResetScore(String line)
  {
    this._scoreboard.resetScores(line);
  }
  
  public String Clean(String line)
  {
    if (line.length() > 16) {
      line = line.substring(0, 16);
    }
    line = HUtil.colorize(line);
    return line;
  }
  
  public void Write(String line)
  {
    line = Clean(line);
    
    this._elements.add(new ScoreboardElementText(line));
  }
  
  public void WriteOrdered(String key, String line, int value, boolean prependScore)
  {
    if (prependScore) {
      line = value + " " + line;
    }
    line = Clean(line);
    for (ScoreboardElement elem : this._elements) {
      if ((elem instanceof ScoreboardElementScores))
      {
        ScoreboardElementScores scores = (ScoreboardElementScores)elem;
        if (scores.IsKey(key))
        {
          scores.AddScore(line, value);
          return;
        }
      }
    }
    this._elements.add(new ScoreboardElementScores(key, line, value, true));
  }
  
  public void WriteBlank()
  {
    this._elements.add(new ScoreboardElementText(" "));
  }
  
  @SuppressWarnings("unchecked")
public void Draw()
  {
    if (this._debug) {
      System.out.println();
    }
    if (this._debug) {
      System.out.println("/////////////////////////");
    }
    ArrayList<String> newLines = new ArrayList<String>();
    for (Iterator<ScoreboardElement> localIterator1 = this._elements.iterator(); localIterator1.hasNext();)
    {
      ScoreboardElement elem = (ScoreboardElement)localIterator1.next();
      
      Iterator<?> localIterator2 = elem.GetLines().iterator(); 

      String line = (String)localIterator2.next();
      boolean matched;
      do
      {
        matched = false;
        for (String otherLine : newLines) {
          if (line.equals(otherLine))
          {
            line = line + ChatColor.RESET;
            matched = true;
          }
        }
      } while (matched);
      newLines.add(line);
    }
    HashSet<Integer> toAdd = new HashSet<>();
    Object toDelete = new HashSet<>();
    for (int i = 0; i < 15; i++) {
      if (i >= newLines.size())
      {
        if (this._current[i] != null)
        {
          if (this._debug) {
            System.out.println("Delete: " + i + " [" + this._current[i] + "]");
          }
          ((HashSet<Integer>)toDelete).add(Integer.valueOf(i));
        }
      }
      else if ((this._current[i] == null) || (!this._current[i].equals(newLines.get(i))))
      {
        if (this._debug) {
          System.out.println("Update: " + i + " [" + (String)newLines.get(i) + "]");
        }
        ((HashSet<Integer>)toDelete).add(Integer.valueOf(i));
        toAdd.add(Integer.valueOf(i));
      }
    }
    for (Iterator<?> localIterator2 = ((HashSet<?>)toDelete).iterator(); localIterator2.hasNext();)
    {
      int i = ((Integer)localIterator2.next()).intValue();
      if (this._current[i] != null)
      {
        if (this._debug) {
          System.out.println("Deleting: " + i + " [" + this._current[i] + "]");
        }
        ResetScore(this._current[i]);
        this._current[i] = null;
      }
    }
    for (Iterator<Integer> localIterator2 = toAdd.iterator(); localIterator2.hasNext();)
    {
      int i = ((Integer)localIterator2.next()).intValue();
      

      String newLine = (String)newLines.get(i);
      GetObjectiveSide().getScore(newLine).setScore(15 - i);
      this._current[i] = newLine;
      if (this._debug) {
        System.out.println("Setting: " + (15 - i) + " [" + newLine + "]");
      }
    }
  }
  
  public void Reset()
  {
    this._elements.clear();
  }
}

