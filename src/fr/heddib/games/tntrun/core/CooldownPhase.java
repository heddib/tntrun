package fr.heddib.games.tntrun.core;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.scheduler.BukkitTask;

import fr.heddib.core.HeddiBCore;
import fr.heddib.core.Util;
import fr.heddib.core.game.Game;
import fr.heddib.core.game.GamePhase;
import fr.heddib.core.game.GameState;
import fr.heddib.core.game.user.User;
import fr.heddib.games.tntrun.messages.MessageHandler;

public class CooldownPhase extends GamePhase {
	
	private Game game;
	private BukkitTask task;
	private boolean running;
	private int time;
	
	public CooldownPhase(Game game) {
		super(game);
		this.game = game;
	}	
	
	public void load() {
		time = game.getCooldownTime();
		game.setState(GameState.COOLDOWN);
		start();
	}
	
	public void start() {
		running = true;
		
		task = Bukkit.getScheduler().runTaskTimer(HeddiBCore.get(), new Runnable() {
			public void run() {
				
				((TNTRun) game).writeScoreboard();
				
				for(User user : game.getUsers()) {
					user.getPlayer().setLevel(time);
					user.getPlayer().setExp(Util.getExpPercent((float)time, (float)game.getCooldownTime()));
				}
				
				if(time > 0 && (time % 5 == 0 || (time <= 10 && time > 0))) {
					game.sendMessage(MessageHandler.getMessage("game-cooldown").replace("%0%", Util.getFormatedTime(time)));
				}
				
				if(time <= 10 && time > 5) {
					game.sendTitle("&a" + time, null, 5, 15, 5);
				}
				
				if(time <= 5 && time > 3) {
					game.sendTitle("&e" + time, null, 5, 15, 5);
				}
				
				if(time <= 3 && time > 0) {
					game.sendTitle("&c" + time, null, 5, 15, 5);
				}
				
				if(time <= 5 && time > 0) {
					for(User user : game.getUsers()) {
						user.getPlayer().playSound(user.getPlayer().getLocation(), Sound.NOTE_STICKS, 8.0F, 1.0F);
					}
				} else if(time == 0) {
					for(User user : game.getUsers()) {
						user.getPlayer().playSound(user.getPlayer().getLocation(), Sound.NOTE_PLING, 8.0F, 1.0F);
						user.clearInventory();
					}
					task.cancel();
					running = false;
					time = game.getCooldownTime();
					game.startIngame();
					return;
				}
				
				game.updateScoreboard();
				time--;
			}
		}, 0L, 20L);
	}
	
	public int getTime() {
		return time;
	}
	
	public void cancelTask() {
		if(task != null)
			task.cancel();
		running = false;
		time = game.getCooldownTime();
	}
	
	public boolean isRunning() {
		return running;
	}
}