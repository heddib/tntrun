package fr.heddib.games.tntrun.core;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import fr.heddib.core.Util;
import fr.heddib.core.game.Game;
import fr.heddib.core.game.GamePhase;
import fr.heddib.games.tntrun.TntRun;
import fr.heddib.games.tntrun.messages.MessageHandler;

public class GameManager {
	
	private List<Game> games = new ArrayList<>();
	private static FileConfiguration cfg;
	
	public GameManager() {
		reinitializeDatabase();
		loadAll();
	}
	
	public static void reinitializeDatabase() {
		cfg = TntRun.get().getConfig();
	}
	
	public void createGame(Player p, String lobbyname) {
		String path = "Games." + lobbyname;
		
		if(cfg.contains(path)) {
			p.sendMessage(MessageHandler.getMessage("game-already-exists").replace("%0%", lobbyname));
			return;
		}
		
		path += ".";
		
		cfg.set(path + "Lobby", Util.serializeLocation(p.getLocation(), true));
		TntRun.get().saveConfig();
		
		p.sendMessage(MessageHandler.getMessage("game-created").replace("%0%", lobbyname));
		p.sendMessage(MessageHandler.getMessage("game-set-spawn").replace("%0%", lobbyname));
		return;
	}
	
	public void setSpawn(Player p, String lobbyname) {
		if(!cfg.contains("Games." + lobbyname)) {
			p.sendMessage(MessageHandler.getMessage("game-not-found").replace("%0%", lobbyname));
			return;
		}
		
		Location loc = p.getLocation();
		
		String s = loc.getWorld().getName() + "," + loc.getX() + "," + loc.getY() + "," + loc.getZ() + "," + loc.getYaw() + "," + loc.getPitch();
		cfg.set("Games." + lobbyname + ".Lobby", s);
		TntRun.get().saveConfig();
		p.sendMessage(MessageHandler.getMessage("game-spawn-set").replace("%0%", lobbyname));
		return;
	}
	
	public void loadAll() { 
		int loaded = 0;
		
		if(cfg.contains("Games")) {
			for(String key : cfg.getConfigurationSection("Games.").getKeys(false)) {
				if(load(key))
					loaded++;
			}
		}
		
		TntRun.get().log(loaded + " lobbys loaded!");
	}
	
	public void unload(Game game) {
		if(game != null) {
			if(game.getPlayingUsers() > 0)
				game.kickall();
			for(GamePhase phase : game.getPhases()) {
				if(phase != null)
					phase.cancelTask();
			}
			games.remove(game);
		}
	}
	
	public boolean load(String name) {
		if(getGame(name) != null) {
			System.out.println("[TNTRun] Lobby " + name + " is already loaded!");
			return false;
		}
		
		String path = "Games." + name;
		
		if(!cfg.contains(path)) {
			System.out.println("[TNTRun] Lobby " + name + " does not exist!");
			return false;
		}
		
		path += ".";
		
		if(!cfg.contains(path + "Lobby")) {
			System.out.println("[TNTRun] The spawn point in lobby " + name + " isn't defined!");
			return false;
		}
		
		Location lobby = Util.parseLocation(cfg.getString(path + "Lobby"));
		
		games.add(new TNTRun(lobby));
		TntRun.get().log("Loaded game: " + name);
		return true;
	}
	
	public List<Game> getGames() {
		return games;
	}
	
	public Game getGame(String name) {
		for(Game game : games) {
			if(game.getName().equalsIgnoreCase(name))
				return game;
		}
		return null;
	}

}
