package fr.heddib.games.tntrun.core;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import fr.heddib.core.game.Game;
import fr.heddib.core.game.GamePhase;
import fr.heddib.core.game.GameState;
import fr.heddib.core.game.user.User;
import fr.heddib.games.tntrun.TntRun;
import fr.heddib.games.tntrun.core.user.UserManager;
import fr.heddib.games.tntrun.messages.MessageHandler;

public class IngamePhase extends GamePhase {
	
	private Game game;
	public BukkitTask task;
	private boolean running;
	private int time;
	private boolean broadcastWin = TntRun.get().getConfig().getBoolean("broadcast-win");
	
	private UserManager um = TntRun.userManger;
	
	// GAME STATISTIC
	// private long start = System.currentTimeMillis();
	// GAME STATISTIC END
	
	public IngamePhase(TNTRun game) {
		super(game);
		this.game = game;
		for(User user : game.getUsers()) {
			user.getPlayer().setAllowFlight(true);
		}
	}
	
	public void load() {
		start();
	}
	
	public void start() {		
		game.setState(GameState.INGAME);
		game.sendMessage(MessageHandler.getMessage("game-start").replace("%0%", Integer.valueOf(game.getPlayingUsers()).toString()));
		running = true;
		game.redefinePlayerNavigatorInventory();
		
		game.getLobby().getWorld().setTime(0);
		
		game.announceGame();
		
		for(User user : game.getUsers()) {
			user.getPlayer().teleport(game.getLobby());
		}

		startTask();		
	}
	
	private void startTask() {
		task = Bukkit.getScheduler().runTaskTimer(TntRun.get(), new Runnable() {
			public void run() {

				game.updateScoreboard();
				time--;
			}
		}, 0L, 20L);
	}
	
	public void killUser(User user, User killer, boolean leave, boolean spectate) {
		int remain = game.getUsers().size() - 1;
		
		game.setPlacements(user.getPlayer());
		
		// STATISTIC
		//user.getStatistics().addPlayed();
		//user.getStatistics().addDeath();
		
		if(leave) {
			game.sendMessage(MessageHandler.getMessage("game-player-left").replace("%0%", user.getName()));
		} else {			
		game.sendMessage(MessageHandler.getMessage("game-player-die").replace("%0%", user.getName()));		
		}
		
		game.sendMessage(MessageHandler.getMessage("game-remainplayers").replace("%0%", Integer.valueOf(remain).toString()));
						
		final Player p = user.getPlayer();
		
		um.leaveGame(p);
		//game.setDeathAmount(game.getDeathAmount() + 1);
		//game.updateScoreboard();
		
		if(remain == 1) {
			User winner = game.getUsers().get(0);
			
			game.setPlacements(winner.getPlayer());
			
			// STATISTIC
			//winner.getStatistics().addWin();
			//winner.getStatistics().addPlayed();
			
			if(broadcastWin) {
				Bukkit.broadcastMessage(MessageHandler.getMessage("game-win").replace("%0%", winner.getName()).replace("%1%", game.getName()));
			}
			
			winner.sendMessage(MessageHandler.getMessage("game-win-winner-message"));
			
			// POINTS
			//winner.addPoints(6);
			
			// ON MUTE LE CHAT VITE FAIS OKLM
			//HeddiBCore.get().getChat().silence(3D, true);
			
			// EUH... ON CLEAR SON INV xD
			winner.clearInventory();
			
			// GAME STATISTIC
			/*DatabaseThread.addTask(new DatabaseTask("INSERT INTO `" + DatabaseManager.tablePrefix + "games` " +
					"(`arena`, `duration`, `end`, `players`, `winner`) " +
					"VALUES ('" + game.getCurrentArena().getName() + "'," +
					"'" + (System.currentTimeMillis() - start) / 1000 + "'," +
					"'" + new Date(System.currentTimeMillis()) + "'," +
					"'" + players + "'," +
					"'" + winner.getPlayer().getUniqueId().toString() + "')"));*/
			// GAME STATISTIC END
			
			game.startFinish();
		} else {
			if(spectate) {
				//if(PermissionHandler.hasPermission(p, Permission.SPECTATE)) {
					Bukkit.getScheduler().scheduleSyncDelayedTask(TntRun.get(), new Runnable() {
						public void run() {
							um.joinGameAsSpectator(p, game.getName());
						}
					}, 2L);
				//}
			}

		}
	}
	
	public int getTime() {
		return time;
	}
	
	public void cancelTask() {
		if(task != null)
			task.cancel();
	}

	public boolean isRunning() {
		return running;
	}
}
