package fr.heddib.games.tntrun;

import org.bukkit.entity.Player;

import fr.heddib.core.command.CommandBase;
import fr.heddib.core.game.GameState;
import fr.heddib.core.util.H;

public class GameCommand extends CommandBase<GamePlugin> {

	public GameCommand(GamePlugin plugin) {
		super(plugin, "game.admin", new String[] { "game", "g" });
	}
	
	public void execute(Player player, String[] args) {
		if(args == null) {
			player.sendMessage(H.main("Game", "Il faut des arguments mon petit."));
		} else if(args.length == 1) {
			if(args[0].equalsIgnoreCase("setlobby") || args[0].equalsIgnoreCase("setspawn")) {
				TntRun.gameManager.setSpawn(player, "TNTRun");
			} else if(args[0].equalsIgnoreCase("create")) {
				TntRun.gameManager.createGame(player, "TNTRun");
			} else if(args[0].equalsIgnoreCase("join")) {
				TntRun.userManger.joinGame(player, "TNTRun");
			} else if(args[0].equalsIgnoreCase("leave")) {
				TntRun.userManger.leaveGame(player);
			} else if(args[0].equalsIgnoreCase("start")) {
				TntRun.gameManager.getGame("TNTRun").forceStart(player);
			} else if(args[0].equalsIgnoreCase("reset")) {
				if(TntRun.userManger.isPlaying(player.getName())) {
					if(TntRun.userManger.getUser(player).getGame().getState() == GameState.INGAME)
						TntRun.userManger.getUser(player).getGame().startFinish();
				}
			}			
		}
	}

}
