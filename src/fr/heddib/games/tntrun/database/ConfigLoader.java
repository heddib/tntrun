package fr.heddib.games.tntrun.database;

import org.bukkit.configuration.file.FileConfiguration;

import fr.heddib.games.tntrun.TntRun;

public class ConfigLoader {
	
	public void load() {
		reloadConfig();
		reloadMessages();
	}

	public static void reloadConfig() {
		TntRun.get().reloadConfig();
		FileConfiguration c = TntRun.get().getConfig();
		
		c.options().header(
				"TntRun par heddib"
				);
		
		c.addDefault("Spectating.Enabled", "true");
		c.addDefault("Spectating.Max-Spectators", 20);
		
		c.addDefault("server", "test");
		
		c.options().copyDefaults(true);
		TntRun.get().saveConfig();
	}
	
	public static void reloadMessages() {
		FileConfiguration c = new YMLLoader("plugins/TNTRun", "messages.yml").getFileConfiguration();
		TntRun.messages = c;
		
		c.addDefault("prefix", "&7[&cTNTRun&7] &6");
		
		c.addDefault("join-unknown-game", "&cThe lobby %0% does not exist!");
		c.addDefault("join-game-running", "&cThis game is already running!");
		c.addDefault("join-vehicle", "&cYou can't join TntRun in a vehicle!");
		c.addDefault("join-game-full", "&cSorry, this lobby is full!");
		c.addDefault("join-success", "%0% joined the game! &7(&e%1%&7/&e%2%&7)");
		c.addDefault("fulljoin-kick", "&cI'm sorry, you've been kicked to make a free slot for a donator or a team member!");
		c.addDefault("join-already-playing", "&cYou're already playing!");
		c.addDefault("leave-not-playing", "&cYou aren't playing!");
		c.addDefault("game-leave", "%0% left the game! &7(&e%1%&7/&e%2%&7)");
		c.addDefault("game-cooldown", "The game starts in &b%0%");
		c.addDefault("game-player-die", "%0% has died!");
		c.addDefault("game-player-left", "%0% left the game!");
		c.addDefault("game-remainplayers", "&b%0%&6 players remain.");
		c.addDefault("game-player-list", "There are %0% players&7: %1%");
		c.addDefault("game-start-canceled", "Not enough players are in this lobby. Cancel Timer...");
		c.addDefault("game-start", "The round begins, &b%0% &6players are playing! &bGood luck&6!");
		c.addDefault("game-win", "%0% won the TNTRun!");
		c.addDefault("game-win-winner-message", "&bCongratulations!&6 You won the TNTRun!");
		c.addDefault("game-end", "&cThe round ends in %0%!");
		c.addDefault("game-already-exists", "&cThe lobby %0% already exist!");
		c.addDefault("game-created", "You've created the lobby %0% successfully!");
		c.addDefault("game-spawn-set", "You've set the spawn for lobby %0% successfully!");
		c.addDefault("game-set-spawn", "To set the spawn of this lobby, type /game setspawn");
		c.addDefault("game-not-found", "&cThe lobby %0% does not exists!");
		c.addDefault("forbidden-command", "&cYou can't execute this command in TNTRun!");
		c.addDefault("forbidden-build", "&cYou aren't allowed to build in a TNTRun arena!");
		c.addDefault("game-double-jump-unavailable", "Double jump unavailable.");
		c.addDefault("game-double-jump-used", "You've used your unique double jump!");
		
		c.options().copyDefaults(true);
		TntRun.saveMessages();
	}

}
