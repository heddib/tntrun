package fr.heddib.games.tntrun.database;

import fr.heddib.core.game.Game;
import fr.heddib.games.tntrun.messages.MessageHandler;

public class ConfigReloader {
	
	public static void reloadConfig() {
		ConfigLoader.reloadConfig();
		Game.reinitializeDatabase();
	}
	
	public static void reloadMessages() {
		ConfigLoader.reloadMessages();
		MessageHandler.reload();
	}

}
