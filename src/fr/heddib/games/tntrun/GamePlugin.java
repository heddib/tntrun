package fr.heddib.games.tntrun;

import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.util.NumberConversions;

import fr.heddib.core.HeddiBPlugin;
import fr.heddib.core.game.Game;
import fr.heddib.core.game.GameState;
import fr.heddib.core.game.user.User;
import fr.heddib.core.plugin.MiniPlugin;
import fr.heddib.core.util.H;
import fr.heddib.core.util.HUtil;
import fr.heddib.games.tntrun.core.IngamePhase;
import fr.heddib.games.tntrun.core.TNTRun;
import fr.heddib.games.tntrun.messages.MessageHandler;

public class GamePlugin extends MiniPlugin {

	public GamePlugin(HeddiBPlugin plugin) {
		super("Game", plugin);
	}
	
	public void addCommands() {
	    addCommand(new GameCommand(this));
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		if(TntRun.gameManager.getGames().isEmpty()) {
			p.sendMessage(H.main("Game", "Il n'y a aucun jeu sur ce serveur, pour en cr�er un &c/game"));
			return;
		}
		if(!TntRun.userManger.isPlaying(p.getName())) {
			if(TntRun.gameManager.getGames().get(0).getState() == GameState.INGAME) {
				
				return;
			}
			TntRun.userManger.joinGame(p, TntRun.gameManager.getGames().get(0).getName());
		}
	}
	
	@EventHandler
	public void onPreLogin(PlayerLoginEvent e) {
		if(!TntRun.gameManager.getGames().isEmpty()) {
            if(TntRun.gameManager.getGames().get(0).getState() == GameState.INGAME) {
            	e.setResult(Result.KICK_OTHER);
				e.setKickMessage(HUtil.colorize("&cGame in progress."));
				return;
			}
		}
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		if(TntRun.userManger.isPlaying(p.getName())) {
			User user = TntRun.userManger.getUser(p.getName());
			Game game = user.getGame();
				
			/*if(game.getState() != GameState.INGAME && game.getState() != GameState.DEATHMATCH) {
				TntRun.userManger.leaveGame(p);
				return;
			}*/
				
			IngamePhase ip = ((TNTRun) game).getIngamePhrase();
			ip.killUser(user, null, true, false);
		}
	}
	
	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent e) {
		e.setFoodLevel(20);
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onEntityDamage(EntityDamageEvent e) {
		if(e.getEntity() instanceof Player) {
			if(!(e.getCause() == DamageCause.VOID)) {
				e.setDamage(0);
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
		if((e.getDamager() instanceof Player) && (e.getEntity() instanceof Player)) {
			e.setDamage(0);
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {
		Player p = e.getPlayer();
		if(TntRun.userManger.isPlaying(p.getName())) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onToggleFlight(PlayerToggleFlightEvent e) {
		Player p = e.getPlayer();
		if(p.getGameMode() == GameMode.CREATIVE || p.getGameMode() == GameMode.SPECTATOR)
			return;
		if(TntRun.userManger.isPlaying(p.getName())) {
			if(!TntRun.userManger.isSpectator(p.getName())) {
				Game game = TntRun.gameManager.getGames().get(0);
				if(game.getState() != GameState.INGAME || game.getState() != GameState.END)
					return;
				e.setCancelled(true);
				p.setAllowFlight(false);
				p.setVelocity(p.getLocation().getDirection().multiply(0.7D).setY(0.5D));
				p.sendMessage(MessageHandler.getMessage("game-double-jump-used"));
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDeathEvent(PlayerDeathEvent event) {
		Player p = event.getEntity();

		if(TntRun.userManger.isPlaying(p.getName())) {
			User user = TntRun.userManger.getUser(p.getName());
			Game game = user.getGame();
			event.setDeathMessage(null);
				
			if(game.getState() != GameState.INGAME && game.getState() != GameState.DEATHMATCH) {
				TntRun.userManger.leaveGame(p);
				return;
			}
				
			IngamePhase ip = ((TNTRun) game).getIngamePhrase();

			event.getDrops().clear();
				
			ip.killUser(user, null, false, true);
		}
	}
	
	/////////////////////////////////
	//     BLOCS REGENERATION      //
	//          BY HEDDIB          //
	/////////////////////////////////
	
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e) {
		Player p = e.getPlayer();
		if(TntRun.userManger.isPlaying(p.getName())) {
			Game game = TntRun.userManger.getUser(p).getGame();
			if(game.getState() != GameState.INGAME || game.isFinishing()) {
				return;
			}
			destroyBlock(e.getPlayer().getLocation());
		}
	}
	
	  private HashSet<Block> blockstodestroy = new HashSet<>();
	  
	  @SuppressWarnings("unused")
	private final int SCAN_DEPTH = 2;
	  
	  public void destroyBlock(Location loc)
	  {
	    int y = loc.getBlockY() + 1;
	    Block block = null;
	    for (int i = 0; i <= 2; i++)
	    {
	      block = getBlockUnderPlayer(y, loc);
	      y--;
	      if (block != null) {
	        break;
	      }
	    }
	    if (block != null)
	    {
	      final Block fblock = block;
	      if (!this.blockstodestroy.contains(fblock))
	      {
	        this.blockstodestroy.add(fblock);
	        Bukkit.getScheduler().scheduleSyncDelayedTask(
	          TntRun.get(), 
	          new Runnable()
	          {
	            @SuppressWarnings("deprecation")
				public void run()
	            {
	               blockstodestroy.remove(fblock);
	               fblock.getWorld().playEffect(fblock.getLocation(), Effect.STEP_SOUND, fblock.getTypeId());
	               removeGLBlocks(fblock);
	            }
	          }, 10L);
	      }
	    }
	  }
	  
	  @SuppressWarnings("deprecation")
	public void regenNow()
	  {
		  long start = System.currentTimeMillis();
	    Iterator<String> bsit = B.iterator();
	    while (bsit.hasNext())
	    {
	      String bl = (String)bsit.next();
	      String[] bd = bl.split(":");
	      
	      int id = Integer.parseInt(bd[0]);
	      byte data = Byte.parseByte(bd[1]);
	      World world = Bukkit.getWorld(bd[2]);
	      int x = Integer.parseInt(bd[3]);
	      int y = Integer.parseInt(bd[4]);
	      int z = Integer.parseInt(bd[5]);
	      
	      world.getBlockAt(x, y, z).setTypeId(id);
	      world.getBlockAt(x, y, z).setData(data);
	    }
	    long time = (start - System.currentTimeMillis()) / 100;
	    TntRun.get().log("Finished arena reset! (" + time + "ms)");
	  }
	  
	  private void removeGLBlocks(Block block)
	  {
	    saveBlock(block);
	    block = block.getRelative(BlockFace.DOWN);
	    saveBlock(block);
	  }
	  
	  private static double PLAYER_BOUNDINGBOX_ADD = 0.3D;
	  
	  private Block getBlockUnderPlayer(int y, Location location)
	  {
	    PlayerPosition loc = new PlayerPosition(location.getX(), y, location.getZ());
	    Block b11 = loc.getBlock(location.getWorld(), PLAYER_BOUNDINGBOX_ADD, -PLAYER_BOUNDINGBOX_ADD);
	    if (b11.getType() != Material.AIR) {
	      return b11;
	    }
	    Block b12 = loc.getBlock(location.getWorld(), -PLAYER_BOUNDINGBOX_ADD, PLAYER_BOUNDINGBOX_ADD);
	    if (b12.getType() != Material.AIR) {
	      return b12;
	    }
	    Block b21 = loc.getBlock(location.getWorld(), PLAYER_BOUNDINGBOX_ADD, PLAYER_BOUNDINGBOX_ADD);
	    if (b21.getType() != Material.AIR) {
	      return b21;
	    }
	    Block b22 = loc.getBlock(location.getWorld(), -PLAYER_BOUNDINGBOX_ADD, -PLAYER_BOUNDINGBOX_ADD);
	    if (b22.getType() != Material.AIR) {
	      return b22;
	    }
	    return null;
	  }
	  
	  @SuppressWarnings("unused")
	private final int MAX_BLOCKS_PER_TICK = 10;
	  private static List<String> B = new LinkedList<>();
	  
	  @SuppressWarnings("deprecation")
	public void saveBlock(Block b)
	  {
	    String block = b.getTypeId() + ":" + b.getData() + ":" + b.getWorld().getName() + 
	      ":" + b.getX() + ":" + b.getY() + ":" + b.getZ();
	    B.add(block);
	    b.setType(Material.AIR);
	  }
	
	  public int regen()
	  {
	    final Iterator<String> bsit = B.iterator();
	    int t = 0;
	    for (int ticks = 1; ticks <= B.size() / 10 + 1; ticks++) {
	      Bukkit.getScheduler().scheduleSyncDelayedTask(TntRun.get(), 
	        new Runnable()
	        {
	          @SuppressWarnings("deprecation")
			public void run()
	          {
	            try
	            {
	              while (bsit.hasNext())
	              {
	                String bl = (String)bsit.next();
	                String[] bd = bl.split(":");
	                
	                int id = Integer.parseInt(bd[0]);
	                byte data = Byte.parseByte(bd[1]);
	                World world = Bukkit.getWorld(bd[2]);
	                int x = Integer.parseInt(bd[3]);
	                int y = Integer.parseInt(bd[4]);
	                int z = Integer.parseInt(bd[5]);
	                
	                world.getBlockAt(x, y, z).setTypeId(id);
	                world.getBlockAt(x, y, z).setData(data);
	              }
	            }
	            catch (ConcurrentModificationException localConcurrentModificationException) {}
	          }
	        }, ticks);
	      t = ticks;
	    }
	    return t;
	  }
	
	  private static class PlayerPosition
	  {
	    private double x;
	    private int y;
	    private double z;
	    
	    public PlayerPosition(double x, int y, double z)
	    {
	      this.x = x;
	      this.y = y;
	      this.z = z;
	    }
	    
	    public Block getBlock(World world, double addx, double addz)
	    {
	      return world.getBlockAt(NumberConversions.floor(this.x + addx), this.y, NumberConversions.floor(this.z + addz));
	    }
	  }

}
