=== REQUIREMENTS ==

* Spigot 1.8.8 ou plus (pas testé en 1.9)
* HeddiBCore version 3.4.x ou plus

=== DESCRIPTION ===

* TNTRun est un jeu d'Hypixel recréé par heddib pour tester ces compétences.
* Version 1.3.5
* [Hypixel TNT Games](https://hypixel.net/tnt-games)

=== SCREENSHOTS ===

Message quand on rejoint la partie:

![unnamed (1).png](http://heddib.tk/static/unnamed1.png)

Démarrage de la partie:

![unnamed (2).png](http://heddib.tk/static/unnamed2.png)

Annonce du jeu:

![unnamed (3).png](http://heddib.tk/static/unnamed3.png)

Fin:

![unnamed.png](http://heddib.tk/static/unnamed.png)